package tracking.id11723222.com.trackingapplication;

/**
 * Created by phealeyhang on 27/10/15.
 */
public class Constants {
    public static final String EXTRA_LOCATION = "location";
    public static final String DELETED = "Entry Deleted";
    public static final String LOCATION_SELECTED = "Location Selected";
    public static final String CURRENT_TIME = "Current time is";
    public static final String NEW_LINE = "\n";
    public static final String EMAIL_ON = "email";
    public static final String STARTED = "Tracking Service Started";
    public static final String ERROR = "Error: ";
    public static final String BLANK_STATE = " ";
    public static final String CLEARED_TEXT = "Cleared";
    public static final String ADD_CLICKED_EXCEPTION = "onAddClicked Exception: ";
    public static final String COMPARED_TO = " compared to ";
    public static final String RECORDED_LOCATIONS = "Recorded Locations";
    public static final String EMAIL_FORMAT = "message/rfc822";
    public static final String MAIL_TO = "mailto:";
    public static final String ENTRY_REASON = "Reason";
    public static final String ENTRY_CREATED = "Entry Created";
    public static final String INTERVAL = "Interval";
    public static final String DURATION = "Duration";
    public static final String TIME = "Time";
    public static final String UPDATE_COMMAND = "Updating";
    public static final String CLEARED = "Recorded times and locations cleared";
    public static final String INTENT_SERVICE_NAME = "Tracking Service";
    public static final float ZOOM_LEVEL = 14.0f;
    public static final int INITIAL_LOCATION = 0;
    public static final int MAX_ADDRESS_VALUE = 1;
    public static final int GAP=10;
    public static final int MAX_LENGTH = 64;
    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int DEFAULT_INTERVAL = 10000;
    public static final int DEFAULT_DURATION = 100000;
    public static final int MILLISECONDS_TO_MINUTES = 10000;
    public static final int MILLISECONDS_TO_SECONDS = 1000;






}
